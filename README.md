Git Tutorial
============

## Git for Version Control 

Git is a version control system. You can download and install it from
[here](https://git-scm.com/download/).  

![git logo](img/git_logo.png) 

It's basic (but not the only) function is to give you a convenient way how to access different versions of 
your code in history. In order to enable git versioning for the directory, where your code 
is located, you have to initialize it as a <b>git repository (repo)</b>. 
This is done by opening the directory in command line and typing 

```
git init
```

This creates file `.git` in the folder where all the versioning information is present. 
The versioning is not done automatically but you have to tell git which files to version. 
There are two steps that you need to follow: 
1. <b>Add</b> files to <b>staging area</b>
2. <b>Commit</b> the files 
This is done by issuing the following commands

```
git add file1.txt
git add file2.txt
...
git add file3.txt

git commit -m "Add some files" 
```

This creates a <b>commit</b>, which is a point in the git history, that you can later return to. 
Basically the `git add` command is used for selecting the files that you want to commit 
and the `git commit` command is used for the actual creation and naming of the commit. 
Use 

```
git status
```

to see which files are added to staging area and which are not.

> The name of the commit should be short and descriptive and in imperative (Add vs Added). It starts with an uppercase letter
> and does <b>not</b> end with any punctuation (./?/!). 

> Add every change in the code that is meaningful by itself alone into a separate
> commit. So if you make a lot of changes at once, you can add them to staging area one by one 
> and make a single commit for each. 
 
> Sometimes it is useful to add all the files at once, this is done by
> 
>```
>add .
>```
> 
> You must be careful not to include some files you don't want to. 
> These are for example <b>automatically generated files or files with passwords</b>. For this purpose it is highly advisable 
> to include `.gitignore` file to the root directory of your project and specify the files 
> you never want to include there. Use [this website](https://www.toptal.com/developers/gitignore) to generate basic gitignore for 
> your programming language (python, R) and IDE (PyCharm, Spyder, RStudio).

To list your commit history you can use

```
git log
```

To see a concrete example: 

![git workflow](img/git_workflow.png) 

- You can then rollback to any commit in history you made and see your changes, but this is in fact not used that often in practice. 
- Git by itself gives you one other very powerful tool that is worth to mention here and those are <b>branches</b>. You can 
create a separate "branch" of your code that you work on and change while someone else is changing the code in other branch. You 
can then easily merge these branches, which is great for a team collaboration. The default branch is called <b>master</b>.
- You don't have to do these commands in the command line. Most IDEs today already have some integration with git.  

## Remote Repositories

One other great feature that is used all the time are <b>remote repositories</b>. These are 
mirror images of your repository that are hosted on some other server. Useful when your computer crashes and 
you loose all the data or <b>when you want to work on your code on different computers.</b> 
The remote repository can in fact be hosted on the same computer in a different folder, but 
the usual way to host it is to use some service. The two most common are 

![gitlab vs github](img/gitlab_vs_github.png)

GitHub is older and more standard. GitLab is newer but has more bells and whistles. 
It is also easier to use in my opinion. 
To have a remote repository for your project you must: 

1. Create it on GitLab: <br><br>
![git gitlab new project](img/gitlab_new_project.png) <br><br> 
Select the same name as you name your directory with code (use lowercase letters, use `-` instead of spaces and no special characters). Set the project visibility.<br><br> 
![gitlab fill project details](img/gitlab_fill_project_details.png)

2. Open a command line in your code directory on your computer and paste the following line 
that GitLab told you when created the project (change the username and repo name). This links the remote repository to your 
local repository and calls it <b>origin</b> (you can have multiple remotes).
    ```
    git remote add origin git@gitlab.com:josef.ondrej/git-tutorial.git
    ``` 
   ![add remote](img/add_remote.png)

Just adding the remote repository does not mean your directory is automatically synchronized. Again, 
you have to do this manually. 

Use 
```
git push -u origin master
```
to push the changes in local branch <b>master</b> to the remote repository called <b>origin</b>. 
You only need to do it with the first push, otherwise using just `git push` is enough.

Use 
```
git pull
```
to download the changes from the remote repository. 

## GitLab SSH keys
GitLab (and GitHub) both support SSH authentication. Long story short - if you don't want to enter 
your password and username each time you push/pull your changes, do this. 

1. Generate SSH private and public key pair by typing 
    ```
    ssh-keygen -t ed25519 -C "<comment>"
    ``` 
    into the command line. Copy the contents of the `.ssh/id_ed25519.pub` file into clipboard.

2. Paste the contents of the clipboard into the add ssh key form on GitLab, which you can find by going into 
Settings > SSH Keys > Add an SSH Key

    ![gitlab settings](img/gitlab_settings.png)

    ![gitlab ssh menu](img/gitlab_ssh_menu.png)
    
    ![gitlab ssh key](img/add_ssh_key.png)
